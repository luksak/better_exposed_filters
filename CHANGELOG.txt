
Better Exposed Filters change log
---------------------------------


Better Exposed Filters 6.x-x.x  xxxx-xx-xx
------------------------------------------
#864614 by OxideInteractive: Fixes extra space in class attributes
#874978 by vosechu: select_as_checkboxes now respects #default_value
#812778 by mikeker: Fixes problem with "Show hierarchy in dropdown enabled"
#811954 by pivica: Fixes duplicate Select All/None links with multiple Behavior executions
#657148 by mikeker: Adds support for exposed sort manipulation
#965388 by mikeker: Adds support for collapsible fieldsets
#965388 by mikeker: Adds support for collapsible fieldsets
mikeker: Adds nested list display for hierarchical taxonomy filters
#894312 by kenorb, mikeker: Adds links as a BEF option
#1006716 by attiks: Corrects <label>/<input> tag order 

Better Exposed Filters 6.x-1.0  2010-05-25
------------------------------------------
Initial release.  Supports Views 2.x and 3.x.